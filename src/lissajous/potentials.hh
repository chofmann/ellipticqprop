// Definitions of potentials

double always_zero2(double t, int me) {
    return 0;
};

double always_zero5(double x, double y, double z, double t, int me) {
    return 0;
};

// The vector potential with sine squared envelope
class vecpot 
{
    double omega1;
    double omega2;
    double  E_1;
    double  E_2;
    double   n_c1;
    double   n_c2;
    double    phi_cep1;
    double    phi_cep2;
    double     tau;
    double       ww1;
    double       ww2;
    double  duration1;
    double  duration2;
    double  duration;
public:
    vecpot(double om1, double om2, double n_cyc1, double n_cyc2, double delay, double E_max1, double E_max2, double cep1, double cep2) : omega1(om1), omega2(om2), n_c1(n_cyc1), n_c2(n_cyc2), tau(delay), E_1(E_max1), E_2(E_max2), phi_cep1(cep1), phi_cep2(cep2)
    {
        duration1=n_c1*2*M_PI/omega1;
        duration2=n_c2*2*M_PI/omega2+tau;
        duration = (duration1>duration2)?duration1:duration2; // Total duration of the (overlaping) pulses
        // Angular frequency of the envelopes
        ww1=omega1/(2.0*n_c1);
        ww2=omega2/(2.0*n_c2);
    };
    double operator()(double time, int me) const 
    {
        double result = 0.0;
        if (time<duration1) 
        {
            // Here's the shape of the laser pulse
            if (E_1!=0.0)
            {
                result += E_1/omega1*pow2(sin(ww1*time))*sin(omega1*time + phi_cep1);
            };
        };
        if ((time>tau)&&(time<duration2))
        {
            if (E_2!=0.0)
            {
                result += E_2/omega2*pow2(sin(ww2*(time-tau)))*sin(omega2*(time-tau) + phi_cep2);
            };
        };
        return result;
    };
    double get_duration() 
    {
        return duration;
    };
    double get_Up() 
    {
        return 0.25*(E_1*E_1)/(omega1*omega1);
    };
    double integral(double Time)
    {
        const double pulse_dur1 = n_c1*2.0*M_PI/omega1;
        const double pulse_dur2 = n_c2*2.0*M_PI/omega2+tau;
        // const double pulse_dur = (pulse_dur1<pulse_dur2)?pulse_dur2:pulse_dur1; // Total duration of the (overlaping) pulses
        const double excursion_ampl1 = E_1/(omega1*omega1);
        const double excursion_ampl2 = E_2/(omega2*omega2);
        // This is the time integral of A(t), i.e., the free-electron excursion alpha(t)
        double result = 0.0;
        if (Time<pulse_dur1)
        {
            if (E_1!=0.0)
            {
                result += 0.5*excursion_ampl1*( 
                                    cos(phi_cep1)*( 1.0-0.5*n_c1/(n_c1+1.0)-0.5*n_c1/(n_c1-1.0) ) 
                                    -cos(omega1*Time+phi_cep1)
                                    +cos(omega1*Time+omega1*Time/n_c1+phi_cep1)*0.5*n_c1/(n_c1+1.0)
                                    +cos(omega1*Time-omega1*Time/n_c1+phi_cep1)*0.5*n_c1/(n_c1-1.0));
            };
        };
        if ((Time>tau)&&(Time<pulse_dur2))
        {
            if (E_2!=0.0)
            {
                result += 0.5*excursion_ampl2*( 
                                    cos(phi_cep2)*( 1.0-0.5*n_c2/(n_c2+1.0)-0.5*n_c2/(n_c2-1.0) ) 
                                    -cos(omega2*(Time-tau)+phi_cep2)
                                    +cos(omega2*(Time-tau)+omega2*(Time-tau)/n_c2+phi_cep2)*0.5*n_c2/(n_c2+1.0)
                                    +cos(omega2*(Time-tau)-omega2*(Time-tau)/n_c2+phi_cep2)*0.5*n_c2/(n_c2-1.0));
            };
        };
        return result;
    };
};

class scalarpot 
{
  double nuclear_charge;
  double R_co;
public:
  scalarpot(double charge, double co) : nuclear_charge(charge), R_co(co) {
    //
  };
  double operator()(double x, double y, double z, double time, int me) const {
    // Scrinzi potential
    // double result=(x<R_co)?nuclear_charge*(-1.0/x-pow2(x)/(2*pow3(R_co))+3.0/(2.0*R_co)):0.0;
    // Simple Volker potential; first -1/r then linear then zero
    const double m=1.0/pow2(R_co);
    double result=(x<R_co)?-1.0/x:((x<2*R_co)?-1.0/R_co+m*(x-R_co):0.0);
    return nuclear_charge*result;
  };
  double get_nuclear_charge() {return nuclear_charge; };
};

class imagpot {
  long imag_potential_width;
  double ampl_im;  // Amplitude of imaginary absorbing potential  <--------------  100.0 imag pot on,  0.0 off
public:
  imagpot(long width, double ampl=100.0) : ampl_im(ampl), imag_potential_width(width) {
    //
  };
  double operator()(long xindex, long yindex, long zindex, double time, grid g) {
    if (ampl_im>1.0) {
      const long imag_start=g.ngps_x()-imag_potential_width;
      if (xindex<imag_start)
	return 0;
      else {
	const double r=double(xindex-imag_start)/double(imag_potential_width);
	return ampl_im*pow2(pow2(pow2(pow2(r))));
      };
    }
    else {
      return 0.0;
    };
  };
};
