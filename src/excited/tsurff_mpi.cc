#include <iostream>
#include <memory>
#include <complex>
#include <time.h>
typedef std::complex<double> cplxd;
typedef std::unique_ptr<cplxd[]> cplxd_ptr;

#include <string>
#ifdef HAVE_MPI
#include "mpi.h"
#endif



#include <tsurffSpectrum.hh>
// Functions determining the potentials are defined in potentials.hh
#include <potentials.hh>

using std::cout;
using std::string;

void print_banner(long tsurff_version)
{
    fprintf(stdout, " ---------------------------------------------------\n");
    if (tsurff_version==1)
        fprintf(stdout, "                 t-SURFF initialized              \n");
    if (tsurff_version==2)
        fprintf(stdout, "                 i-SURFV initialized              \n");
    fprintf(stdout, " ---------------------------------------------------\n");
};

int main(int argc, char **argv) {
#ifdef HAVE_MPI
    clock_t t = clock();
    int ierr = MPI_Init(&argc, &argv);
    cout << "MPI_Init returns " << ierr << endl;
    int i_proc;
    MPI_Comm_rank(MPI_COMM_WORLD, &i_proc);
#endif
    parameterListe para_ini("initial.param");
    parameterListe para_prop("propagate.param");
    parameterListe para_tsurff("tsurff.param");
    
    const long tsurff_version = para_tsurff.getLong("tsurff-version");
    const long     qprop_dim  = para_ini.getLong("qprop-dim");
    if (i_proc==0) 
    {
        print_banner(tsurff_version);
    };
    double omega1 = para_prop.getDouble("omega-1");
    double omega2 = para_prop.getDouble("omega-2");
    
    double E_1x     = para_prop.getDouble("max-electric-field-1-x");
    double E_2x     = para_prop.getDouble("max-electric-field-2-x");
    double E_1y     = para_prop.getDouble("max-electric-field-1-y");
    double E_2y     = para_prop.getDouble("max-electric-field-2-y");
    
    double phase1x  = para_prop.getDouble("phase-1-x");
    double phase2x  = para_prop.getDouble("phase-2-x");
    double phase1y  = para_prop.getDouble("phase-1-y");
    double phase2y  = para_prop.getDouble("phase-2-y");
    
    double n_c1 = para_prop.getDouble("num-cycles-1");
    double n_c2 = para_prop.getDouble("num-cycles-2");
    
    double tau = para_prop.getDouble("tau-delay");
    

    // To exclude the Y-axis-field parameters if linear polarization is used
    if (qprop_dim==34)
    {
        E_1y = 0.0;
        E_2y = 0.0;
    };
    // To avoid errors in case of zero field strength
    if ((E_1x==0.0)&&(E_1y==0.0)) 
    {
        omega1 = 1.0;
        n_c1   = 0.0;
    };
    if ((E_2x==0.0)&&(E_2y==0.0))
    {
        omega2 = 1.0;
        n_c2   = 0.0;
        tau    = 0.0;
    };
    if (((omega1==0.0)&&((E_1x!=0.0)||(E_1y!=0.0)))||((omega2==0.0)&&((E_2x!=0.0)||(E_2y!=0.0))))
    {
        cout << " -----------------------------------------------------------\n";
        cout << " err: Zero frequency for a non-zero electric field detected.\n Exiting program...\n";
        cout << " -----------------------------------------------------------\n";
        exit(1);
    };
    
    double E1x,E1y,E1z, E2x,E2y,E2z, n1x,n1y,n1z, n2x,n2y,n2z;
    // Only A_z in linear polarization case, renaming Ex1,2 -> Ez1,2
    if (qprop_dim == 34) 
    {
        E1x = 0.0;
        E1y = 0.0;
        E1z = E_1x;
        E2x = 0.0;
        E2y = 0.0;
        E2z = E_2x;
        n1x = 0.0;
        n1y = 0.0;
        n1z = n_c1;
        n2x = 0.0;
        n2y = 0.0;
        n2z = n_c2;
    } 
    // Only A_x and A_y in XY-plane polarization case
    else if (qprop_dim == 44) 
    {
        E1x = E_1x;
        E1y = E_1y;
        E1z = 0.0;
        E2x = E_2x;
        E2y = E_2y;
        E2z = 0.0;
        n1x = n_c1;
        n1y = n_c1;
        n1z = 0.0;
        n2x = n_c2;
        n2y = n_c2;
        n2z = 0.0;
    };
    vecpot vecpot_x(omega1, omega2, n1x, n2x, tau, E1x, E2x, phase1x, phase2x);
    vecpot vecpot_y(omega1, omega2, n1y, n2y, tau, E1y, E2y, phase1y, phase2y);
    vecpot vecpot_z(omega1, omega2, n1z, n2z, tau, E1z, E2z, phase1x, phase2x);
    
    tsurffSpectrum<vecpot, vecpot, vecpot> tsurff(para_ini, para_prop, para_tsurff, vecpot_x, vecpot_y, vecpot_z);
    tsurff.time_integration();
    // tsurff.print_int_dt_psi();
    // tsurff.print_wigner(0);
    // tsurff.print_Plms();
    // tsurff.print_bessel();
    tsurff.polar_spectrum();
    tsurff.print_partial_amplitudes();
#ifdef HAVE_MPI
    int ierrfin = MPI_Finalize();
    float sec = ((float)(clock() - t))/CLOCKS_PER_SEC;
    cout << "Timer: " << long(sec)/3600 << "h " << long(sec)%3600/60 << "min " << (long(sec*100)%6000)/100.0 << "sec\n";
    fprintf(stdout, " ---------------------------------------------------\n");
    if (tsurff_version==1)
        fprintf(stdout, "t-SURFF finished on proc %d. MPI_Finalize returns %d \n", i_proc, ierrfin );
    if (tsurff_version==2)
        fprintf(stdout, "i-SURFV finished on proc %d. MPI_Finalize returns %d \n", i_proc, ierrfin );
    fprintf(stdout, " ---------------------------------------------------\n");
#endif
    return 0;
};