Example: Hydrogen, lambda=400 nm and 240 nm, I=5 10^13 Wcm^-2 each, circular counter-rotating polarizations in XY-plane

Quickstart:
./do_all.sh

or 

Step-by-step:
make clean
make imag_prop
./imag_prop
make real_prop
./real_prop
make isurfv
./isurfv
make tsurff
./tsurff

Remarks:
"make clean" leads to a fresh compilation of the qprop library. If you run into problems during compilation or execution try this first.

"make imag_prop" builds the program for calculationg the initial state by imaginary propagation

"make real_prop" builds the program for the real time propagtion.

"make isurfv" builds the program for calculating the laser-free part of the surface flux, that is only necessary if the i-SURFV method is used ("tsurff-version long 2" in the "tsurff.param" file)

"make tsurff" builds the program for computing the spectrum from the data produced during the real time propagation.

A parallel version of the surface flux calculation program can be compiled and executed

make tsurff_mpi
mpirun -np Np tsurff_mpi > log.tsurff

"Np" refers to a number of processes used.
It was tested with the openmpi library but using a different mpi library should be possible.
If you want to use a different mpi library or have your openmpi library in a non standard location you need to modify the "Makefile".

Every process writes results to its own file so these files need to be joined after the calculation. Use

cat tsurff_polar0.dat tsurff_polar1.dat  > tsurff_polar.dat
or
cat tsurff_polar*.dat > tsurff_polar.dat

Concatenate the files in the correct order (increasing process number) or you're gonna have a bad time.
Alternativly use "< cat $(ls -v tsurff_polar*.dat)" as the file name in your gnuplot script.

To visualize the photoelectron spectrum, use the Python script "plot_pes.py"

To visualize the laser time dependence and/or geometry use the Python script "plot_laser.py"