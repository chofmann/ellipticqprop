This is a Qprop release for calculating electron spectra of single electron systems in intense laser fields using the i-SURFV or t-SURFF approximation.

Prerequisites for using the package:
-GNU Scientific Library (gsl versions 1.16 and 2.0 have been tested)
-GNU C++ compiler supporting the C++11 standard (Versions 4.7.2, 4.8.4 and 5.3.0 have been tested)

Optional:
-Open MPI (parallelized version of spectrum evaluation)

If libraries are in non standard locations, the Makefiles in the example directories and in src/base have to be modified.

Several examples are provided in the ./src/ subdirectory.

Plotting Python scripts are located in the plots/ subdirectory. 
Additional libraries required for plotting: NumPy, Matplotlib, SciPy (only if angle-resolved spectra are to be built from partial spectra).
